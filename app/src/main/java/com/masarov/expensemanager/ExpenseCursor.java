package com.masarov.expensemanager;

import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.ArrayList;

public class ExpenseCursor extends CursorWrapper {

    public ExpenseCursor(Cursor cursor) {
        super(cursor);
    }

    public ArrayList<Expense> getExpenses() {
        if (isBeforeFirst() && isAfterLast()) {
            return new ArrayList<Expense>();
        }
        ArrayList<Expense> list = new ArrayList<Expense>();
        while (moveToNext()) {
            list.add(new Expense(
                    getInt(getColumnIndex(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_ID)),
                    getDouble(getColumnIndex(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_AMOUNT)),
                    getString(getColumnIndex(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_DESCRIPTION)),
                    getInt(getColumnIndex(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_CATEGORY)),
                    getInt(getColumnIndex(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_YEAR)),
                    getInt(getColumnIndex(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_MONTH)),
                    getInt(getColumnIndex(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_DAY)),
                    getInt(getColumnIndex(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_HOUR)),
                    getInt(getColumnIndex(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_MINUTES))
            ));
        }
        return list;
    }

    public ArrayList<String> getGroups(){
        if (isBeforeFirst() && isAfterLast()) {
            return new ArrayList<String>();
        }
        ArrayList<String> list = new ArrayList<String>();
        while(moveToNext()){
            String month = Integer.toString(getInt(getColumnIndex(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_MONTH)));
            String year = Integer.toString(getInt(getColumnIndex(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_YEAR)));
            list.add(year + (month.length() < 2 ? "0" + month : month));
        }
        return list;

    }


}
