package com.masarov.expensemanager;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.*;


public class HistoryExpandableListAdapter extends BaseExpandableListAdapter{

    private Context mContext;
    private List<Map.Entry<String, List<Expense>>> mData;
    private Fragment mParentFragment;

    public HistoryExpandableListAdapter(Context context, TreeMap<String, List<Expense>> expensesMap, Fragment parentFragment){
        mContext = context;
        mParentFragment = parentFragment;
        mData = new ArrayList<Map.Entry<String, List<Expense>>>();
        for(Map.Entry<String, List<Expense>> entry : expensesMap.entrySet()){
            mData.add(entry);
        }
    }

    @Override
    public int getGroupCount() {
        return mData.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mData.get(groupPosition).getValue().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mData.get(groupPosition).getValue();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mData.get(groupPosition).getValue().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return mData.get(groupPosition).getValue().get(childPosition).getId();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.history_list_month, null);
        }
        TextView periodName = (TextView) convertView.findViewById(R.id.period_name);
        TextView periodSum = (TextView) convertView.findViewById(R.id.period_sum);

        double sum = 0;
        for(Expense expense : mData.get(groupPosition).getValue()){
            sum += expense.getAmount();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, mData.get(groupPosition).getValue().get(0).getMonth());
        calendar.set(Calendar.YEAR, mData.get(groupPosition).getValue().get(0).getYear());

        periodName.setText(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " / " + calendar.get(Calendar.YEAR));
        periodSum.setText(String.format("%.2f", sum));
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.history_list_expense, null);
        }

        Expense child = mData.get(groupPosition).getValue().get(childPosition);

        TextView categoryColor = (TextView) convertView.findViewById(R.id.category_color_history);
        categoryColor.setBackgroundColor(Color.parseColor(Category.getCategory(child.getCategoryNumber()).getColor()));

        TextView categoryName = (TextView) convertView.findViewById(R.id.category_name_history);
        String textForCategoryName;
        if(child.getDescription() != null && !"".equals(child.getDescription())){
            textForCategoryName =  child.getDescription().trim();
        }else{
            textForCategoryName = Category.getCategory(child.getCategoryNumber()).getName();
        }
        categoryName.setTextColor(Color.parseColor(Category.getCategory(child.getCategoryNumber()).getColor()));
        categoryName.setText(textForCategoryName);

        TextView expenseDate = (TextView) convertView.findViewById(R.id.expense_date_history);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, child.getMonth());
        calendar.set(Calendar.DAY_OF_MONTH, child.getDay());
        calendar.set(Calendar.HOUR_OF_DAY, child.getHour());
        calendar.set(Calendar.MINUTE, child.getMinutes());
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE / dd.LL / kk:mm");
        expenseDate.setText(dateFormat.format(calendar.getTime()));

        TextView expense = (TextView) convertView.findViewById(R.id.expense_sum_history);
        expense.setText(String.format("%.2f", child.getAmount()));


        ImageButton deleteButton = (ImageButton) convertView.findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteExpenseDialog dialog = new DeleteExpenseDialog(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == Dialog.BUTTON_NEGATIVE){
                            dialog.dismiss();
                        }else{
                            BusProvider.getInstance().post(new ExpenseActionPerformedEvent(
                                    ExpenseActionPerformedEvent.Type.DELETE,
                                    mData.get(groupPosition).getValue().get(childPosition)));
                            mData.get(groupPosition).getValue().remove(childPosition);
                            notifyDataSetChanged();
                        }
                    }
                });
                dialog.show(mParentFragment.getFragmentManager(), "deleteExpenseDialog");
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
