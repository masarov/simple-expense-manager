package com.masarov.expensemanager;

public enum Category {

    Other(0, "Other", "#999999"),
    Health(1, "Health", "#F02311"),
    Shopping(2, "Shopping", "#1693A5"),
    Travel(3, "Travel", "#604830"),
    Bills(4, "Bills", "#77CCA4"),
    Leisure(5, "Leisure", "#FF7200");

    private final int id;
    private final String name;
    private final String color;

    Category(int id, String name, String color){
        this.id = id;
        this.name = name;
        this.color = color;
    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getColor(){
        return color;
    }

    public static Category getCategory(int position){
        switch (position){
            case 0:
                return Category.Other;
            case 1:
                return Category.Health;
            case 2:
                return Category.Shopping;
            case 3:
                return Category.Travel;
            case 4:
                return Category.Bills;
            case 5:
                return Category.Leisure;
            default:
                return Category.Other;
        }
    }
}
