package com.masarov.expensemanager;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.*;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.masarov.expensemanager.graphs.*;
import com.squareup.otto.Subscribe;

import java.util.*;


public class StatsFragment extends Fragment implements DialogInterface.OnClickListener,
                                                       LineGraph.OnPointClickedListener{

    @Bind(R.id.graph_bar) BarGraph mBarGraph;
    @Bind(R.id.graph_line) LineGraph mLineGraph;
    @Bind(R.id.stats_no_expenses_message) TextView mTextViewNoStats;

    private Calendar mCalendar;
    private ArrayList<Expense> mListOfExpenses;
    private HashSet<Integer> mListOfCategories;
    private double mMaxDailySum;
    private ExpensesReloadedEvent mEvent;

    @Subscribe public void onExpensesReloaded(ExpensesReloadedEvent event){
        if(event.getGroupedExpenses() == null || event.getGroupedExpenses().size() < 1){
            mTextViewNoStats.setText("It seems that you don't have any expenses yet.");
            mBarGraph.setAlpha(0);
        }else{
            mEvent = event;
            removeOldDataAndCreateNew(event.getListOfExpenses(), true);
        }
    }

    @Override public void onResume(){
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override public void onPause(){
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_stats, viewGroup, false);
    }

    @Override public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        ButterKnife.bind(this, getActivity());
        mCalendar = Calendar.getInstance();
        setHasOptionsMenu(true);
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.stats_menu, menu);
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        DialogFragment periodPicker = new PeriodPickerFragment(this, mEvent.getListOfExpenseGroups());
        periodPicker.show(getFragmentManager(), "PeriodPickerFragment");
        return true;
    }

    @Override public void onClick(DialogInterface dialog, int which) {
        if (which == Dialog.BUTTON_NEGATIVE){
            dialog.dismiss();
        }else {
            removeOldDataAndCreateNew(mEvent.getGroupedExpenses().get(mEvent.getListOfExpenseGroups().get(which)), false);
        }
    }

    private void removeOldDataAndCreateNew(Collection<Expense> expenses, boolean currentMonthOnly) {
        mLineGraph.removeAllLines();
        mBarGraph.removeAllBars();
        gatherExpensesAndGroups(expenses, currentMonthOnly);
        createLineGraph();
        createBarGraph();
        mTextViewNoStats.setText(mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " / " + mCalendar.get(Calendar.YEAR));
    }

    private void gatherExpensesAndGroups(Collection<Expense> expenses, boolean currentMonthOnly) {
        mListOfExpenses = new ArrayList<Expense>();
        mListOfCategories = new HashSet<Integer>();
        if(!currentMonthOnly && expenses.size() > 0){
            mCalendar = Calendar.getInstance();
            if( !(mCalendar.get(Calendar.MONTH) == expenses.iterator().next().getMonth() &&
                    mCalendar.get(Calendar.YEAR) == expenses.iterator().next().getYear()) ){
                mCalendar.set(Calendar.MONTH, expenses.iterator().next().getMonth());
                mCalendar.set(Calendar.YEAR, expenses.iterator().next().getYear());
                mCalendar.set(Calendar.DAY_OF_MONTH, mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            }

        }
        for(Expense expense : expenses){
            if( (!currentMonthOnly) || ( expense.getMonth() == mCalendar.get(Calendar.MONTH) && expense.getYear() == mCalendar.get(Calendar.YEAR)) ){
                mListOfExpenses.add(expense);
                mListOfCategories.add(expense.getCategoryNumber());
            }
        }
    }

    private void createLineGraph() {
        mMaxDailySum = 0;
        Line line = new Line();
        line.setColor(Color.parseColor("#FFBB33"));
        for(int i = 1; i <= mCalendar.get(Calendar.DAY_OF_MONTH); i++){
            double dailySum = 0;
            for(Expense expense : mListOfExpenses){
                if(expense.getDay() == i){
                    dailySum += expense.getAmount();
                }
            }
            line.addPoint(new LinePoint(i, (float) dailySum));
            if(dailySum > mMaxDailySum){
                mMaxDailySum = dailySum;
            }
        }
        mLineGraph.setOnPointClickedListener(this);
        mLineGraph.addLine(line);
        mLineGraph.setLineToFill(0);
        mLineGraph.setGridColor(Color.BLUE);
        mLineGraph.setClickable(true);
        mLineGraph.setRangeY(0, (float) mMaxDailySum + 10);
    }

    private void createBarGraph() {
        ArrayList<Bar> points = new ArrayList<Bar>();
        for(Integer i : mListOfCategories){
            Category category = Category.getCategory(i);
            Bar bar = new Bar();
            double sum = 0;
            for(Expense expense : mListOfExpenses){
                if(expense.getCategoryNumber() == i){
                    sum += expense.getAmount();
                }
            }
            bar.setColor(Color.parseColor(category.getColor()));
            bar.setName(category.getName());
            bar.setValue((float) sum);
            points.add(bar);
        }
        mBarGraph.setBars(points);
        mBarGraph.setPadding(0, 20, 0, 0);
        mBarGraph.setClickable(true);
        mBarGraph.setAlpha(1);
    }

    @Override public void onClick(int lineIndex, int pointIndex) {
        Line line = mLineGraph.getLine(lineIndex);
        LinePoint point = line.getPoint(pointIndex);
        ToastGenerator.generateNeutralToast(getActivity(), ((int)point.getX()) + " of " + mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + ", Spent " + String.format("%.2f",point.getY())).show();
    }
}
    

