package com.masarov.expensemanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;


public class ExpenseDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "expense.sqlite";
    private static final int VERSION = 2;

    private static final String SQL_CREATE_TABLE = "CREATE TABLE " + ExpenseEntry.TABLE_NAME + " ( "+
            ExpenseEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            ExpenseEntry.COLUMN_AMOUNT + " REAL," +
            ExpenseEntry.COLUMN_DAY + " INTEGER," +
            ExpenseEntry.COLUMN_YEAR + " INTEGER," +
            ExpenseEntry.COLUMN_HOUR + " INTEGER," +
            ExpenseEntry.COLUMN_MONTH + " INTEGER," +
            ExpenseEntry.COLUMN_MINUTES + " INTEGER," +
            ExpenseEntry.COLUMN_CATEGORY + " INTEGER," +
            ExpenseEntry.COLUMN_DESCRIPTION + " TEXT)";

    private static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + ExpenseEntry.TABLE_NAME;

    public ExpenseDatabaseHelper(Context context){
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_TABLE);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL(SQL_DROP_TABLE);
        onCreate(db);
    }

    public static abstract class ExpenseEntry implements BaseColumns{
        public static final String TABLE_NAME = "expense";
        public static final String COLUMN_ID = "expense_id";
        public static final String COLUMN_DAY = "day";
        public static final String COLUMN_YEAR = "year";
        public static final String COLUMN_HOUR = "hour";
        public static final String COLUMN_MONTH = "month";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_MINUTES = "minutes";
        public static final String COLUMN_CATEGORY = "category";
        public static final String COLUMN_DESCRIPTION = "description";
    }

}
