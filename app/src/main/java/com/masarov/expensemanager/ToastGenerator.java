package com.masarov.expensemanager;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ToastGenerator {

    public static Toast generatePositiveToast(Activity activity, String text){
        return generateToast(R.layout.positive_toast_layout, activity, text);
    }

    public static Toast generateNegativeToast(Activity activity, String text){
        return generateToast(R.layout.negative_toast_layout, activity, text);
    }

    public static Toast generateNeutralToast(Activity activity, String text){
        return generateToast(R.layout.neutral_toast_layout, activity, text);
    }

    private static Toast generateToast(int layoutId, Activity activity, String text){
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(layoutId,
                (ViewGroup) activity.findViewById(R.id.toast_layout_root));

        TextView textView = (TextView) layout.findViewById(R.id.text);
        textView.setText(text);

        Toast toast = new Toast(activity);
        toast.setGravity(Gravity.BOTTOM, 0, 5);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);

        return toast;
    }



}
