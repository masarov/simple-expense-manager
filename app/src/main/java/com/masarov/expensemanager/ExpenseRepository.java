package com.masarov.expensemanager;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ExpenseRepository {

    private ExpenseDatabaseHelper helper;

    public void setHelper(ExpenseDatabaseHelper helper) {
        this.helper = helper;
    }

    public long saveExpense(Expense expense){
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = prepareContentValues(expense);
        return db.insert(ExpenseDatabaseHelper.ExpenseEntry.TABLE_NAME, null, values);
    }

    public ArrayList<Expense> getExpenses(){
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(ExpenseDatabaseHelper.ExpenseEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                ExpenseDatabaseHelper.ExpenseEntry.COLUMN_YEAR  + " ASC, "
                        + ExpenseDatabaseHelper.ExpenseEntry.COLUMN_MONTH + " ASC, "
                        + ExpenseDatabaseHelper.ExpenseEntry.COLUMN_DAY + " ASC, "
                        + ExpenseDatabaseHelper.ExpenseEntry.COLUMN_HOUR + " ASC, "
                        + ExpenseDatabaseHelper.ExpenseEntry.COLUMN_MINUTES + " ASC",
                null);
        ExpenseCursor expenseCursor = new ExpenseCursor(cursor);
        return expenseCursor.getExpenses();
    }

    public ArrayList<String> getGroups(){
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(true, ExpenseDatabaseHelper.ExpenseEntry.TABLE_NAME,
                new String[]{ExpenseDatabaseHelper.ExpenseEntry.COLUMN_YEAR,
                        ExpenseDatabaseHelper.ExpenseEntry.COLUMN_MONTH},
                null,
                null,
                null,
                null,
                ExpenseDatabaseHelper.ExpenseEntry.COLUMN_YEAR  + " ASC, "
                        + ExpenseDatabaseHelper.ExpenseEntry.COLUMN_MONTH + " ASC, "
                        + ExpenseDatabaseHelper.ExpenseEntry.COLUMN_DAY + " ASC, "
                        + ExpenseDatabaseHelper.ExpenseEntry.COLUMN_HOUR + " ASC, "
                        + ExpenseDatabaseHelper.ExpenseEntry.COLUMN_MINUTES + " ASC",
                null);
        ExpenseCursor expenseCursor = new ExpenseCursor(cursor);
        return expenseCursor.getGroups();
    }

    private ContentValues prepareContentValues(Expense expense) {
        ContentValues values = new ContentValues();
        values.put(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_YEAR, expense.getYear());
        values.put(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_MONTH, expense.getMonth());
        values.put(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_DAY, expense.getDay());
        values.put(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_MINUTES, expense.getMinutes());
        values.put(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_AMOUNT, expense.getAmount());
        values.put(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_HOUR, expense.getHour());
        values.put(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_CATEGORY, expense.getCategoryNumber());
        values.put(ExpenseDatabaseHelper.ExpenseEntry.COLUMN_DESCRIPTION, expense.getDescription());
        return values;
    }

    public int deleteExpense(int expenseId){
        SQLiteDatabase db = helper.getWritableDatabase();
        return  db.delete(ExpenseDatabaseHelper.ExpenseEntry.TABLE_NAME, ExpenseDatabaseHelper.ExpenseEntry.COLUMN_ID + "=" + expenseId, null);
    }


}
