package com.masarov.expensemanager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ListAdapter;


public class CategoryPickerFragment extends DialogFragment {

    private DialogInterface.OnClickListener mListener;

    public CategoryPickerFragment(DialogInterface.OnClickListener listener){
        mListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListAdapter adapter = new CategoryListAdapter<String>(getActivity(), R.layout.categry_list_row);
        builder.setNegativeButton("Cancel", mListener);
        builder.setTitle("Select category")
                .setAdapter(adapter, mListener);
        return builder.create();
    }
}
