package com.masarov.expensemanager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;


public class DeleteExpenseDialog extends DialogFragment {

    DialogInterface.OnClickListener mListener;

    public DeleteExpenseDialog(DialogInterface.OnClickListener listener){
        mListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Delete expense ?")
                .setPositiveButton("Yes", mListener)
                .setNegativeButton("Cancel", mListener);

        return builder.create();
    }
}
