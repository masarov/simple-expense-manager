package com.masarov.expensemanager;

import android.app.Fragment;
import android.os.Bundle;
import android.view.*;
import android.widget.ExpandableListView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.squareup.otto.Subscribe;


public class HistoryListFragment extends Fragment {

    @Bind(R.id.expandable_history) ExpandableListView mExpandableListView;
    @Bind(R.id.history_no_expenses_message) TextView mTextViewNoHistory;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
    }

    @Override public void onActivityCreated(Bundle bundle){
        super.onActivityCreated(bundle);
        ButterKnife.bind(this, getActivity());
    }

    @Override public void onResume(){
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override public void onPause(){
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe public void onExpensesReloaded(ExpensesReloadedEvent event) {
        if(event.getGroupedExpenses() == null || event.getGroupedExpenses().size() < 1){
            mTextViewNoHistory.setText("It seems that you don't have any expenses yet.");
        }else if (event.getType() == ExpensesReloadedEvent.Type.INITIAL_RELOAD){
            mExpandableListView.setAdapter(new HistoryExpandableListAdapter(getActivity(), event.getGroupedExpenses(), this));
            mTextViewNoHistory.setText("");
        }
    }


}
