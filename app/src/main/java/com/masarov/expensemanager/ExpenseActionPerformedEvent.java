package com.masarov.expensemanager;


public class ExpenseActionPerformedEvent {

    private final Type mType;
    private final Expense mExpense;

    public ExpenseActionPerformedEvent(Type type, Expense expense){
        mType = type;
        mExpense = expense;
    }

    public Type getType(){
        return mType;
    }

    public Expense getExpense(){
        return mExpense;
    }

    public enum Type{
        DELETE,
        ADD
    }
}
