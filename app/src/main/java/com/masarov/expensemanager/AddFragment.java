package com.masarov.expensemanager;

import android.app.*;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;


public class AddFragment extends Fragment implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener, DialogInterface.OnClickListener {

    @Bind(R.id.input_amount) EditText mAmountInput;
    @Bind(R.id.input_description) EditText mDescriptionInput;
    @Bind(R.id.input_category) Button mCategoryInputButton;
    @Bind(R.id.input_date) Button mDateInputButton;
    @Bind(R.id.input_time) Button mTimeInputButton;

    private Expense mExpense;
    private boolean mIsDateSet;
    private boolean mIsTimeSet;
    private int mSelectedCategory;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_add, container, false);
    }

    @Override public void onResume(){
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override public void onPause(){
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }


    @Override public void onActivityCreated(Bundle bundle){
        super.onActivityCreated(bundle);
        ButterKnife.bind(this, getActivity());
        mExpense = new Expense();
        setHasOptionsMenu(true);
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_menu, menu);
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_add){
            addExpense();
            return true;
        }
        return false;
    }

    @Override public void onClick(DialogInterface dialog, int which) {
        if (which == Dialog.BUTTON_NEGATIVE){
            dialog.dismiss();
        }else{
            mCategoryInputButton.setText(Category.getCategory(which).getName());
            mSelectedCategory = which;
        }
    }

    @Override public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mDateInputButton.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
        mExpense.setYear(year);
        mExpense.setMonth(monthOfYear);
        mExpense.setDay(dayOfMonth);
        mIsDateSet = true;
    }

    @Override public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mTimeInputButton.setText(hourOfDay + ":" + minute);
        mExpense.setMinutes(minute);
        mExpense.setHour(hourOfDay);
        mIsTimeSet = true;
    }

    @OnClick(R.id.input_date) public void showDatePicker(){
        DialogFragment datePicker = new DatePickerFragment(this);
        datePicker.show(getFragmentManager(), "datePickerFragment");
    }

    @OnClick(R.id.input_time) public void showTimePicker(){
        DialogFragment timePicker = new TimePickerFragment(this);
        timePicker.show(getFragmentManager(), "timePickerFragment");
    }

    @OnClick(R.id.input_category) public void showCategoryPicker(){
        DialogFragment categoryPicker = new CategoryPickerFragment(this);
        categoryPicker.show(getFragmentManager(), "categoryPickerFragment");
    }

    public void addExpense(){
        try{
            validateAndSetAmount();
        }
        catch (Exception e){
            mAmountInput.setError("Required");
            mAmountInput.requestFocus();
            return;
        }
        setExpenseDate();
        mExpense.setDescription(mDescriptionInput.getText().toString());
        mExpense.setCategoryNumber(mSelectedCategory);
        BusProvider.getInstance().post(new ExpenseActionPerformedEvent(ExpenseActionPerformedEvent.Type.ADD, mExpense));
        resetStateOfInputFields();
    }

    private void validateAndSetAmount() {
        String amountInput = mAmountInput.getText() != null ? mAmountInput.getText().toString() : null;
        double amount = Double.parseDouble(amountInput);
        mExpense.setAmount(round(amount));
    }

    private double round(double value) {
        BigDecimal bigDecimal = new BigDecimal(value);
        bigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);
        return  bigDecimal.doubleValue();
    }

    private void setExpenseDate() {
        Calendar calendar = Calendar.getInstance();
        if (!mIsDateSet) {
            mExpense.setYear(calendar.get(Calendar.YEAR));
            mExpense.setMonth(calendar.get(Calendar.MONTH));
            mExpense.setDay(calendar.get(Calendar.DAY_OF_MONTH));
        }
        if (!mIsTimeSet) {
            mExpense.setMinutes(calendar.get(Calendar.MINUTE));
            mExpense.setHour(calendar.get(Calendar.HOUR_OF_DAY));
        }
    }

    private void resetStateOfInputFields() {
        mExpense = new Expense();
        mAmountInput.setText("");
        mDescriptionInput.setText("");
        mDateInputButton.setText("Today");
        mTimeInputButton.setText("Just now");
        mCategoryInputButton.setText("Other");
        mSelectedCategory = 0;
        mAmountInput.requestFocus();
        mIsDateSet = false;
        mIsTimeSet = false;
    }
}
