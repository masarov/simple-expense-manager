package com.masarov.expensemanager;

public class Expense {

    private double mAmount;
    private int mId;
    private String mDescription;
    private int mCategory;
    private int mYear;
    private int mMonth;
    private int mDay;
    private int mMinute;
    private int mHour;

    public Expense(){}

    public Expense(int id, double amount, String description, int categoryNumber, int year, int month, int day, int hour, int minute){
        mAmount = amount;
        mDescription = description;
        mCategory = categoryNumber;
        mId = id;
        mYear = year;
        mMonth = month;
        mDay = day;
        mHour = hour;
        mMinute = minute;
    }

    public int getMinutes() {
        return mMinute;
    }

    public void setMinutes(int minute) {
        mMinute = minute;
    }

    public int getHour(){
        return mHour;
    }

    public void setHour(int hour){
        mHour = hour;
    }

    public int getDay() {
        return mDay;
    }

    public void setDay(int day) {
        mDay = day;
    }

    public int getYear() {
        return mYear;
    }

    public void setYear(int year) {
        this.mYear = year;
    }

    public int getMonth() {
        return mMonth;
    }

    public void setMonth(int month) {
        this.mMonth = month;
    }

    public int getCategoryNumber() {
        return mCategory;
    }

    public void setCategoryNumber(int categoryNumber) {
        mCategory = categoryNumber;
    }

    public int getId() {
        return mId;
    }

    public double getAmount() {
        return mAmount;
    }

    public void setAmount(double amount) {
        mAmount = amount;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    @Override
    public String toString(){
        return mAmount + ", " + mDescription + ", " + mYear + "/" + mMonth + ", " + mHour + ":" + mMinute;
    }

    public String getGroup(){
        return Integer.toString(mYear) + (mMonth > 9 ? Integer.toString(mMonth) : "0" + Integer.toString(mMonth));
    }
}
