package com.masarov.expensemanager;


import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class ExpensesReloadedEvent {

    private final ArrayList<Expense> mListOfExpenses;
    private final ArrayList<String> mListOfExpenseGroups;
    private final TreeMap<String, List<Expense>> mGroupedExpenses;
    private final Type mType;

    public ExpensesReloadedEvent(ArrayList<Expense> listOfExpenses,
                                 ArrayList<String> listOfExpenseGroups,
                                 TreeMap<String, List<Expense>> groupedExpenses,
                                 Type type){

        mListOfExpenses = listOfExpenses;
        mListOfExpenseGroups = listOfExpenseGroups;
        mGroupedExpenses = groupedExpenses;
        mType = type;
    }

    public Type getType(){
        return mType;
    }

    public ArrayList<Expense> getListOfExpenses() {
        return mListOfExpenses;
    }

    public ArrayList<String> getListOfExpenseGroups() {
        return mListOfExpenseGroups;
    }

    public TreeMap<String, List<Expense>> getGroupedExpenses() {
        return mGroupedExpenses;
    }

    public enum Type{
        INITIAL_RELOAD,
        RELOAD_AFTER_DELETE
    }
}
