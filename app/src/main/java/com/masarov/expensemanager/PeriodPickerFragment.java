package com.masarov.expensemanager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ListAdapter;

import java.util.ArrayList;


public class PeriodPickerFragment extends DialogFragment {

    DialogInterface.OnClickListener mListener;
    ArrayList<String> mPeriods;

    public PeriodPickerFragment(DialogInterface.OnClickListener listener, ArrayList<String> periods){
        mListener = listener;
        mPeriods = periods;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListAdapter adapter = new PeriodListAdapter<String>(getActivity(), R.layout.period_list_row, mPeriods);
        builder.setNegativeButton("Cancel", mListener);
        builder.setTitle("Choose period")
                .setAdapter(adapter, mListener);
        return builder.create();
    }

}
