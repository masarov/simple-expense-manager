package com.masarov.expensemanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class PeriodListAdapter<String> extends ArrayAdapter<String> {

    ArrayList<String> mPeriods;

    public PeriodListAdapter(Context context, int textViewResourceId, ArrayList<String> periods) {
        super(context, textViewResourceId);
        mPeriods = periods;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null){
            convertView = inflater.inflate(R.layout.period_list_row, null);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.period_title);
        try{
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, Integer.parseInt(mPeriods.get(position).toString().substring(0, 4)));
            calendar.set(Calendar.MONTH, Integer.parseInt(mPeriods.get(position).toString().substring(4)));
            textView.setText(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " / " + calendar.get(Calendar.YEAR));
        }catch (NumberFormatException e){
            textView.setText(mPeriods.get(position).toString());
        }
        return convertView;
    }

    @Override
    public int getCount(){
        return mPeriods.size();
    }
}
