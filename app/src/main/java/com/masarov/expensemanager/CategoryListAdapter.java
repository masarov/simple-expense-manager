package com.masarov.expensemanager;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class CategoryListAdapter<String> extends ArrayAdapter<String> {

    public CategoryListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView == null){
            final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.categry_list_row, null);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.category_title);
        textView.setText(Category.getCategory(position).getName());
        TextView color = (TextView) convertView.findViewById(R.id.category_color);
        color.setBackgroundColor(Color.parseColor(Category.getCategory(position).getColor()));
        return convertView;
    }

    @Override
    public int getCount(){
        return Category.values().length;
    }
}
