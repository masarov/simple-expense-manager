package com.masarov.expensemanager;

import android.app.Fragment;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;
import android.app.FragmentManager;
import android.util.Log;
import android.view.ViewGroup;
import com.masarov.expensemanager.viewpager.JazzyViewPager;


public class TabsPagerAdapter extends FragmentPagerAdapter {

    public static final int ADD_FRAGMENT = 0;
    public static final int HISTORY_FRAGMENT = 1;
    public static final int STATS_FRAGMENT = 2;
    private static final String TAG = "TabsPagerAdapter";
    private static final int NUM_OF_TABS = 3;
    private Context mContext;
    private JazzyViewPager mViewPager;

    public TabsPagerAdapter(FragmentManager fm, JazzyViewPager viewPager){
        super(fm);
        mContext = HostActivity.getContext();
        mViewPager = viewPager;
    }

    @Override
    public android.app.Fragment getItem(int position) {
        Log.i(TAG, "getItem(), position: " + position);
        switch (position){
            case ADD_FRAGMENT:
                return new AddFragment();
            case HISTORY_FRAGMENT:
                return new HistoryListFragment();
            case STATS_FRAGMENT:
                return new StatsFragment();
            default:
                return new Fragment();
        }
    }

    @Override
    public int getCount() {
        return NUM_OF_TABS;
    }

    @Override
    public CharSequence getPageTitle(int position){
        Log.i(TAG, "getPageTitle(), position: " + position);
        switch (position){
            case 0:
                return mContext.getString(R.string.title_section_add);
            case 1:
                return mContext.getString(R.string.title_section_history);
            case 2:
                return mContext.getString(R.string.title_section_stats);
            default:
                return "";
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position){
        Object item = super.instantiateItem(container, position);
        mViewPager.setObjectForPosition(item, position);
        return item;
    }
}
