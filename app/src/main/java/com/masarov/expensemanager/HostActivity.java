package com.masarov.expensemanager;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.astuetz.PagerSlidingTabStrip;
import com.masarov.expensemanager.viewpager.JazzyViewPager;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;


public class HostActivity extends Activity implements
        ViewPager.OnPageChangeListener {

    private static Context mContext;
    private ExpenseRepository mExpenseRepository;
    private TreeMap<String, List<Expense>> mGroupedExpenses;
    private ArrayList<Expense> mListOfExpenses;
    private ArrayList<String> mListOfExpenseGroups;
    private TabsPagerAdapter mTabsPagerAdapter;
    private JazzyViewPager mViewPager;
    private PagerSlidingTabStrip mTabStrip;
    private ColorDrawable mCurrentActionBarColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host);

        mExpenseRepository = new ExpenseRepository();
        ExpenseDatabaseHelper helper = new ExpenseDatabaseHelper(this);
        mExpenseRepository.setHelper(helper);
        mContext = this;

        mViewPager = (JazzyViewPager) findViewById(R.id.pager);
        mViewPager.setTransitionEffect(JazzyViewPager.TransitionEffect.CubeOut);
        mTabsPagerAdapter = new TabsPagerAdapter(getFragmentManager(), mViewPager);
        mViewPager.setAdapter(mTabsPagerAdapter);

        mTabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        mTabStrip.setShouldExpand(true);
        mTabStrip.setAllCaps(false);
        mTabStrip.setViewPager(mViewPager);
        mTabStrip.setOnPageChangeListener(this);

        mCurrentActionBarColor = new ColorDrawable(R.color.green);
        mTabStrip.setIndicatorColor(getResources().getColor(R.color.green));

        reloadExpensesFromDatabase();
    }

    public static Context getContext(){
        return mContext;
    }

    public ArrayList<Expense> getListOfExpenses() {
        return mListOfExpenses;
    }

    public ArrayList<String> getListOfExpenseGroups() {
        return mListOfExpenseGroups;
    }

    public TreeMap<String, List<Expense>> getGroupedExpenses() {
        return mGroupedExpenses;
    }

    public void saveExpenseToDatabase(Expense expense) {
        try {
            mExpenseRepository.saveExpense(expense);
            ToastGenerator.generatePositiveToast(this, "Expense has been added !").show();
            reloadExpensesFromDatabase();
        } catch (Exception e) {
            ToastGenerator.generateNegativeToast(this, "Error while adding expense to database!");
        }
    }

    public void reloadExpensesFromDatabase() {
        mListOfExpenseGroups = mExpenseRepository.getGroups();
        if(mListOfExpenseGroups == null || mListOfExpenseGroups.size() == 0){
            // Display Message
            return;
        }
        mGroupedExpenses = new TreeMap<String, List<Expense>>(new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                int first = Integer.parseInt(lhs);
                int second = Integer.parseInt(rhs);
                return first == second ? 0 :
                        first > second ? 1 : -1;
            }
        });
        for(String group : mListOfExpenseGroups){
            mGroupedExpenses.put(group, new ArrayList<Expense>());
        }
        mListOfExpenses = mExpenseRepository.getExpenses();
        for(Expense expense : mListOfExpenses){
            if(mGroupedExpenses.containsKey(expense.getGroup())){
                (mGroupedExpenses.get(expense.getGroup())).add(expense);
            }
        }
        BusProvider.getInstance().post(produceExpensesReloadedEvent());
    }

    @Override public void onResume(){
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override public void onPause(){
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Produce public ExpensesReloadedEvent produceExpensesReloadedEvent(){
        return new ExpensesReloadedEvent(mListOfExpenses, mListOfExpenseGroups,
                                         mGroupedExpenses,
                                         ExpensesReloadedEvent.Type.INITIAL_RELOAD);
    }

    @Subscribe public void reactOnEvent(ExpenseActionPerformedEvent event){
        if(event.getType() == ExpenseActionPerformedEvent.Type.ADD){
            saveExpenseToDatabase(event.getExpense());
        }else if(event.getType() == ExpenseActionPerformedEvent.Type.DELETE){
            deleteExpense(event.getExpense());
        }
    }

    public void deleteExpense(Expense expense) {
        int affectedRows = mExpenseRepository.deleteExpense(expense.getId());
        if(affectedRows > 0){
            mListOfExpenses.remove(expense);
            BusProvider.getInstance().post(new ExpensesReloadedEvent(mListOfExpenses,
                                                                     mListOfExpenseGroups,
                                                                     mGroupedExpenses,
                                                                     ExpensesReloadedEvent.Type.RELOAD_AFTER_DELETE));
            ToastGenerator.generateNegativeToast(this, "Expense has been removed !").show();
        }
    }

    @Override public void onPageScrolled(int i, float v, int i2) {}

    @Override public void onPageSelected(int tabPosition) {
        invalidateOptionsMenu();
        int newColor ;
        switch (tabPosition){
            case 0: newColor = getResources().getColor(R.color.green);
                    break;
            case 1: newColor = getResources().getColor(R.color.blue);
                    break;
            case 2: newColor = getResources().getColor(R.color.violet);
                    break;
            default: newColor = getResources().getColor(R.color.green);
        }
        ColorDrawable[] colorTransition = {mCurrentActionBarColor, new ColorDrawable(newColor)};
        TransitionDrawable trans = new TransitionDrawable(colorTransition);
        trans.startTransition(300);

        getActionBar().setBackgroundDrawable(trans);
        mTabStrip.setIndicatorColor(newColor);
        mCurrentActionBarColor = new ColorDrawable(newColor);

        if(tabPosition != TabsPagerAdapter.ADD_FRAGMENT){
            InputMethodManager inputMethodManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            View amountView = findViewById(R.id.input_amount);
            if(amountView != null){
                amountView.clearFocus();
                inputMethodManager.hideSoftInputFromWindow(amountView.getWindowToken(), 0);
            }
        }
    }

    @Override public void onPageScrollStateChanged(int i) {}
}
